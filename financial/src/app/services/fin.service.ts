import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FinService {
  // declare models for this class
  fullURL = `http://localhost:8080/api/trades`

  constructor(private http:HttpClient) { }

  
//get request to API  endpoint 
  getStocks(){
    
    return this.http.get(this.fullURL)
   
  }

// make a POST to an API end-point
doPOST(data){
  console.log(data)
  let httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
    })
  }
  return this.http.post(this.fullURL, data,httpOptions)
}
}
