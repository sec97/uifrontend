import { Component, OnInit } from '@angular/core';
import { FinService } from '../services/fin.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  // model for this class
  code:String = 'IBM'
  data:any = [{}]
  num:number = 5
  isShow = true
  x:String = 'BUY'


  constructor(private myFinService:FinService) { }

  // button handler 
  // getDataFromCode() {
  //   this.myFinService.getStocks(this.code, this.num)
  //   .subscribe ( (response)=>{this.data = response} )
  // }

  getDataFromCode() {
    this.isShow = !this.isShow;
    this.myFinService.getStocks()
      .subscribe ( (response)=>{this.data = response} )

  }

  ngOnInit(): void {
    // this.getDataFromCode()
    // when this component loads, run this call to an observable method
    // this.myFinService.getStocks()
    // we subscribe to the observable with a call back function
      // .subscribe ( (response)=>{this.data = response} )
    // this.myFinService.doPOST({name:'Alex'})
    //   .subscribe( (r)=>{console.log(r)})
  } 

}
