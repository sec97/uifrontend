import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { FinService } from '../services/fin.service'
import { Trade } from '../trade';
import { JsonPipe } from '@angular/common';
 

@Component({
  selector: 'app-trade-input',
  templateUrl: './trade-input.component.html',
  styleUrls: ['./trade-input.component.css']
})
export class TradeInputComponent implements OnInit {
  sideSelect = ['BUY','SELL']
  tradeModel = new Trade();
  constructor(public fb: FormBuilder,private http: HttpClient,private myFinService:FinService) {
    

  }

  ngOnInit(): void {
  }
  submitForm() {



    
    this.myFinService.doPOST(this.tradeModel)
    .subscribe(
      data =>console.log("Success!", data),
      error => console.log("Error!", error)
    
    )
  }

}
