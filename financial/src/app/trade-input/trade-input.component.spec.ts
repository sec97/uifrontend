import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeInputComponent } from './trade-input.component';

describe('TradeInputComponent', () => {
  let component: TradeInputComponent;
  let fixture: ComponentFixture<TradeInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
